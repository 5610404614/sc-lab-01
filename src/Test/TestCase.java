package Test;

import GUI.Frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.CashCard;
import Test.TestCase;
import Test.TestCase.ListenerMgr;

public class TestCase {
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new TestCase();
	}
	
	public TestCase() {
		frame = new Frame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		CashCard card = new CashCard(0);
		card.checkBalance();
		frame.setResult("Your balance is "+ card.toString());
		
		double refillAmount = 100;
		card.getRefill(refillAmount);
		frame.extendResult("Refill: " + refillAmount + " baht. " + "\nYour balance is "+ card.toString());
		
		double purchaseAmount = 60;
		card.getPurchase(purchaseAmount);
		frame.extendResult("Pay: " + purchaseAmount + " baht. " + "\nYour balance is " + card.toString());
	
		double refillAmount2 = 200;
		card.getRefill(refillAmount2);
		frame.extendResult("Refill: " + refillAmount2 + " baht. " + "\nYour balance is "+ card.toString());
		
		double purchaseAmount2 = 95;
		card.getPurchase(purchaseAmount2);
		frame.extendResult("Pay: " + purchaseAmount2 + " baht. " + "\nYour balance is " + card.toString());

	}

	ActionListener list;
	Frame frame;

}
