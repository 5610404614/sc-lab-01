package GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Frame extends JFrame {

		private JLabel showLabName;
		private JTextArea showResults;
		private JButton endButton;
		private String str;

		public Frame() {
			createFrame();
		}

		public void createFrame() {
			this.showLabName = new JLabel("Cash Card");
			this.showResults = new JTextArea();
			this.endButton = new JButton("end program");
			setLayout(new BorderLayout());
			add(this.showLabName, BorderLayout.NORTH);
			add(this.showResults, BorderLayout.CENTER);
			add(this.endButton, BorderLayout.SOUTH);
		}

		public void setProjectName(String s) {
			showLabName.setText(s);
		}

		public void setResult(String str) {
			this.str = str + "\n";
			showResults.setText(this.str);
		}

		public void extendResult(String str) {
			this.str = this.str + "\n" + str + "\n ";
			showResults.setText(this.str);
		}
	

		public void setListener(ActionListener list) {
			endButton.addActionListener(list);
		}

	}


