package model;

public class CashCard {
	private double balance;
	private String showbalance; 
	
	public CashCard(double balance){ 
		this.balance = 0; 
	}
	public double checkBalance(){ 
		return balance; 
	}
	public void getRefill(double amount){
		
		balance = balance + amount ;
	}
	public void getPurchase(double amount){
		
		balance = balance - amount ;
	}
	public void setShowBalance(String str) {
		showbalance = str;
	}
	public String toString() {
		return  balance + "  Baht.";
	}


}